module.exports = function(app) {

	var messageController = require('../controllers/message');
	var userController = require('../controllers/user');

	app.route('/api/message/:id?')
		.get(messageController.get)
		.post(userController.ensureLoggedIn, messageController.create);

}