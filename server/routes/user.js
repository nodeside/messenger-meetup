var passport = require('passport');



module.exports = function(app) {

	var userController = require('../controllers/user');

	app.get('/api/user/logout', userController.logout);

	app.route('/api/user')
		.get(userController.authenticate)
		.post(userController.register, passport.authenticate('local'), userController.authenticate);

	app.post('/api/user/login', passport.authenticate('local'), userController.authenticate);
}