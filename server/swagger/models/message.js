exports.models = {
  Message: {
    id: 'Message',
    required: ['title', 'creator'],
    properties: {      
      title: {
        type: 'string',
        description: 'Title of the message'
      },
      creator: {
        type: 'User',
        description: 'Creator of the content'
      }
    }
  }
};