var fs = require('fs');
var express = require('express');

var swagger, defaultGetParams, paramTypes;

module.exports = function(app, config) {

	if (swagger) return {
		add: add
	}

	swagger = require('swagger-node-express').createNew(app);

	var paramTypes = swagger.paramTypes;
	var sortParm = paramTypes.query('sort', 'Comma seperated list of params to sort by.  (e.g "-created,name") ', 'string');
	var limitParm = paramTypes.query('limit', 'Number of items to return', 'number');
	var skipParm = paramTypes.query('skip', 'Number of items to skip', 'number');

	defaultGetParams = [
		sortParm,
		limitParm,
		skipParm
	];

	app.use('/swagger/assets', express.static(__dirname + '/assets'));
	app.get('/api/docs', function(req, res, next) {

		fs.readFile(__dirname+'/ui.html', function(err, buffer) {
			res.setHeader("Content-Type", "text/html");
			res.send(buffer.toString());
		});
	});


	config.swagger = config.swagger || {};

	swagger.configureDeclaration(config.app.name, {
		description: config.swagger.description || 'Messenger Meetup API',
		authorizations: ['oauth2'],
		produces: ['application/json']
	});


	swagger.setApiInfo({
		title: config.swagger.title || 'Messenger Meetup',
		description: config.swagger.description || 'Messenger Meetup API',
		termsOfServiceUrl: config.swagger.tos || 'http://sometermsofserviceurl.com',
		contact: config.swagger.contact || 'support@example.com',
		licenseUrl: config.swagger.license || 'http://en.wikipedia.org/wiki/MIT_License'
	});

	swagger.setAuthorizations({
		apiKey: {
			type: 'apiKey',
			passAs: 'header'
		}
	});

	swagger.configureSwaggerPaths('', 'api/swagger/docs', '');
	swagger.configure('/api', '1.0.0');

	return {
		add: add
	}

}

function add(path) {
	var model = require(__dirname + '/models/' + path);
	swagger.addModels(model);

	console.log('loading docs for: ' + path);

	require(__dirname + '/services/' + path)
		.load(swagger, {
			searchableOptions: defaultGetParams
		});

	swagger.configureSwaggerPaths('', 'api/swagger/docs', '');
	swagger.configure('/api', '1.0.0');
};