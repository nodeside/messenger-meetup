'use strict';

exports.load = function(swagger, parms) {

    var searchParms = parms.searchableOptions;

    var list = {
        'spec': {
            description: 'Message operations',
            path: '/message',
            method: 'GET',
            summary: 'Get all messages',
            notes: '',
            type: 'Message',
            nickname: 'getMessages',
            produces: ['application/json'],
            params: searchParms
        }
    };

    var create = {
        'spec': {
            description: 'Message Operations',
            path: '/message',
            method: 'POST',
            summary: 'Create message',
            notes: 'User must be logged in to create a message',
            type: 'Message',
            nickname: 'createMessage',
            produces: ['application/json'],
            parameters: [{
                name: 'body',
                description: 'Message to create. User will be inferred by the authenticated user.',
                required: true,
                type: 'Message',
                paramType: 'body',
                allowMultiple: false
            }]
        }
    };

    swagger.addGet(list)
        .addPost(create);

};