var express = require('express');

var mongoose = require('mongoose');

var bodyParser = require('body-parser');

var cookieParser = require('cookie-parser')

var session = require('express-session');

var MongoStore = require('connect-mongo')(session);

var config = require('./config');

var app = express();

mongoose.connect('mongodb://127.0.0.1:27017/messenger-meetup', function(err) {
	if (err) {
		console.log(err);
	}
});

app.use(bodyParser({
	limit: '50mb'
}));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(cookieParser());

app.use(bodyParser());

app.use(session({
	secret: process.env.SESSION_SECRET || 'something other ppl shouldnt know',
	store: new MongoStore({
		mongooseConnection: mongoose.connection
	})
}));

var swagger = require('./swagger')(app,config);
app.use('/swagger/assets', express.static(__dirname+'/swagger/assets'));

swagger.add('message');


require('./passport')(app);

require('./routes/user')(app);
require('./routes/message')(app);

app.listen(3005);
