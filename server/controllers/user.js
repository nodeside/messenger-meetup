var User = require('../models/user');

module.exports = function() {

	return {
		authenticate: authenticate,
		logout: logout,
		register: register,
		ensureLoggedIn: ensureLoggedIn
	}

	function ensureLoggedIn(req, res, next) {
		if (!req.user) {
			return res.send(401);
		}

		next();
	}

	function authenticate(req, res, next) {
		if (!req.user) {
			return res.send(401);
		}

		res.send({
			username: req.user.username
		});
	}

	function logout(req, res, next) {
		req.logout();
		res.redirect('/')
	}

	function register(req, res, next) {

		User.register(new User({
			username: req.body.username,
			email: req.body.email,
		}), req.body.password, function(err) {
			if (err) {
				return res.send(400, err.message || 'oops');
			}

			next();
		});
	}
}();