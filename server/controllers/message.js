var Message = require('../models/message');

module.exports = function() {

	return {
		get: get,
		create: create
	}

	function get(req, res, next) {
		
		var query = Message.find().populate('creator', 'username');

		if (req.params.id) {
			query.where('_id', req.params.id);
		}

		query.exec(function(err, messages) {

			if (err) {
				return res.send(400, err.message);
			}

			return res.send(messages);
		})
	}

	function create(req, res, next) {

		var message = new Message({
			creator: req.user._id,
			title: req.body.title
		});

		message.save(function(err) {
			if (err) {
				return res.send(400, err.message);
			}

			res.send(200);
		})
	}
}();