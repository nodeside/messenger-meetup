var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Message = new Schema({

	creator: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	title: String
	
});

module.exports = mongoose.model('Message', Message);